<%@ page import="java.sql.Connection" %>
<%@ page import="com.jiat.web.db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: user
  Date: 4/1/2023
  Time: 7:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<h1>Hello</h1>

<h4>Enter Details</h4>

<form action="logintest" method="POST">

    <span>Mobile</span>
    <input type="text" name="mobile">

    <br/><br/>

    <span>Name</span>
    <input type="text" name="name">

    <br/><br/>

    <span>Password</span>
    <input type="password" name="password">

    <br/><br/>
    <input type="submit" value="Submit">

</form>

<br/><br/>

<h4>Registered Users</h4>

<table>

    <tr>
        <th> ID</th>
        <th> Mobile</th>
        <th> Name</th>
        <th> Password</th>
    </tr>
    <%
        Connection connection = null;
        try {
            connection = DBConnection.getConnection();
            ResultSet resultSet = DBConnection.search("SELECT * FROM `user`");
            while (resultSet.next()) {
    %>
    <tr>
        <td>
                <%= resultSet.getString("id")%>
        <td>
        <td>
            <%= resultSet.getString("mobile")%>
        </td>
        <td>
            <%= resultSet.getString("uname")%>
        </td>
        <td>
            <%= resultSet.getString("password")%>
        </td>
    </tr>
    <%
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    %>

</table>
</body>
</html>
